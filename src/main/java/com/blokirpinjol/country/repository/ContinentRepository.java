package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.Continent;
 
/***  ContinentRepository */
public interface ContinentRepository extends JpaRepository<Continent, Integer> {

}
