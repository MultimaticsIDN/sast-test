package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.Province;

public interface ProvinceRepository   extends JpaRepository<Province, Integer>{

}
