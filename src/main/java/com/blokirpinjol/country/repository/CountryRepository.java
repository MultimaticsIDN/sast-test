package com.blokirpinjol.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blokirpinjol.country.model.Country;

public interface CountryRepository extends JpaRepository<Country, Integer>  {
    
}
