package com.blokirpinjol.country;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeBlokirpinjolCountryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeBlokirpinjolCountryApplication.class, args);
	}

}
