package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.Province;
import com.blokirpinjol.country.repository.ProvinceRepository; 

@Service
public class ProvinceService {
	
Logger logger = LoggerFactory.getLogger(ProvinceService.class);
    
	@Autowired
	private ProvinceRepository repository;
	
	public Province save(Province province) {
		province.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(province);
	}
	
	public List<Province> getAll(){
		return repository.findAll();
	}
	
	public Optional<Province> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public Province update(Integer id,Province province) {
		Optional<Province> provincecek = repository.findById(id);
		if(provincecek.isPresent()) { 
			if(province.getName() != null ) provincecek.get().setName(province.getName());
			if(province.getCountry_id() != null ) provincecek.get().setCountry_id(province.getCountry_id());
			province.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(provincecek.get()); 
		}else {
			return provincecek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<Province> province = repository.findById(id);
		if(province.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
