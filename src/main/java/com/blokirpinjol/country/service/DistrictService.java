package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.District;
import com.blokirpinjol.country.repository.DistrictRepository; 

@Service
public class DistrictService {
	
Logger logger = LoggerFactory.getLogger(DistrictService.class);
    
	@Autowired
	private DistrictRepository repository;
	
	public District save(District district) {
		district.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(district);
	}
	
	public List<District> getAll(){
		return repository.findAll();
	}
	
	public Optional<District> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public District update(Integer id,District district) {
		Optional<District> districtcek = repository.findById(id);
		if(districtcek.isPresent()) {  
			if(district.getName() != null ) districtcek.get().setName(district.getName());
			if(district.getCity_id() != null ) districtcek.get().setCity_id(district.getCity_id());
			districtcek.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(districtcek.get()); 
		}else {
			return districtcek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<District> district = repository.findById(id);
		if(district.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
