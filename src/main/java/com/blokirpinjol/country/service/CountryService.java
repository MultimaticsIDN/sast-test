package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.Country;
import com.blokirpinjol.country.repository.CountryRepository; 

@Service
public class CountryService {
	
Logger logger = LoggerFactory.getLogger(CountryService.class);
    
	@Autowired
	private CountryRepository repository;
	
	public Country save(Country country) {
		country.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(country);
	}
	
	public List<Country> getAll(){
		return repository.findAll();
	}
	
	public Optional<Country> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public Country update(Integer id,Country country) {
		Optional<Country> countrycek = repository.findById(id);
		if(countrycek.isPresent()) {  
			if(country.getName() != null ) countrycek.get().setName(country.getName());
			if(country.getIso2() != null ) countrycek.get().setIso2(country.getIso2());
			if(country.getIso3() != null ) countrycek.get().setIso3(country.getIso3());
			if(country.getPopulation() != null ) countrycek.get().setPopulation(country.getPopulation());
			if(country.getCapital() != null ) countrycek.get().setCapital(country.getCapital());
			if(country.getPhone_prefix() != null ) countrycek.get().setPhone_prefix(country.getPhone_prefix());
			if(country.getCapital() != null ) countrycek.get().setCapital(country.getCapital());
			if(country.getContinent_id() != null ) countrycek.get().setContinent_id(country.getContinent_id());
			countrycek.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(countrycek.get()); 
		}else {
			return countrycek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<Country> country = repository.findById(id);
		if(country.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
