package com.blokirpinjol.country.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blokirpinjol.country.model.City;
import com.blokirpinjol.country.repository.CityRepository; 

@Service
public class CityService {
	
Logger logger = LoggerFactory.getLogger(CityService.class);
    
	@Autowired
	private CityRepository repository;
	
	public City save(City city) {
		city.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return repository.save(city);
	}
	
	public List<City> getAll(){
		return repository.findAll();
	}
	
	public Optional<City> getByid(Integer id) {
		return repository.findById(id);
	}
	
	public City update(Integer id,City city) {
		Optional<City> citycek = repository.findById(id);
		if(citycek.isPresent()) { 
			if(city.getName() != null ) citycek.get().setName(city.getName());
			if(city.getProvince_id() != null ) citycek.get().setProvince_id(city.getProvince_id());
			citycek.get().setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return repository.save(citycek.get()); 
		}else {
			return citycek.get();
		}
	}
	
	public Boolean delete(Integer id) {
		Optional<City> city = repository.findById(id);
		if(city.isPresent()) {
			repository.deleteById(id);
			return true;
		}else {
			return false;
		}
	}
}
