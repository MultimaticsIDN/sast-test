package com.blokirpinjol.country.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blokirpinjol.country.model.Province;
import com.blokirpinjol.country.service.ProvinceService;

@RestController
@RequestMapping("/api/province")
public class ProvinceController {
	Logger logger = LoggerFactory.getLogger(ProvinceController.class);
    
    @Autowired
    private ProvinceService service;
    
	@PostMapping
    public ResponseEntity<Province> saveCity(@RequestBody Province province) {
		Province hasil = new Province();
    	try {
    		hasil = service.save(province);
    		return new ResponseEntity<Province>(hasil, HttpStatus.CREATED);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Province>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    }  
	
	@GetMapping
    public ResponseEntity<List<Province>> getAllCity() {
		List<Province> data = new ArrayList<Province>();
    	try {
    		data = service.getAll();
    		return new ResponseEntity<List<Province>>(data, HttpStatus.OK);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<List<Province>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@GetMapping("/{id}")
    public ResponseEntity<Optional<Province>> getByIdCity(@PathVariable Integer id) {
		Optional<Province> data = null;
    	try {
    		data = service.getByid(id);
    		if(data.isPresent()) {
        		return new ResponseEntity<Optional<Province>>(data, HttpStatus.OK);
    		}else { 
        		return new ResponseEntity<Optional<Province>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Optional<Province>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> deleteByIdCity(@PathVariable Integer id) {
		Map<String, Object> data =  new HashMap();
		Boolean hasil = false;
    	try {
    		hasil = service.delete(id);
    		if(hasil) {
    			data.put("message", "Deleted Successfully");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.OK);
    		}else { 
    			data.put("message", "Data Not Found");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
			data.put("message", e.getMessage());
    		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/update/{id}")
    public ResponseEntity<Province> updateCity(@PathVariable Integer id, @RequestBody Province province) {
		Province hasil = new Province();
    	try {
    		hasil = service.update(id,province);
    		if(hasil != null) { 
        		return new ResponseEntity<Province>(hasil, HttpStatus.OK);
    		}else {  
        		return new ResponseEntity<Province>(hasil, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Province>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 

}
