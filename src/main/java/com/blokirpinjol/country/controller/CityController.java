package com.blokirpinjol.country.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blokirpinjol.country.model.City;
import com.blokirpinjol.country.service.CityService; 

@RestController
@RequestMapping("/api/city")
public class CityController { 
    Logger logger = LoggerFactory.getLogger(CityController.class);
    
    @Autowired
    private CityService service;
    
	@PostMapping
    public ResponseEntity<City> saveCity(@RequestBody City city) {
		City city2 = new City();
    	try {
    		city2 = service.save(city);
    		return new ResponseEntity<City>(city2, HttpStatus.CREATED);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<City>(city2, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    }  
	
	@GetMapping
    public ResponseEntity<List<City>> getAllCity() {
		List<City> data = new ArrayList<City>();
    	try {
    		data = service.getAll();
    		return new ResponseEntity<List<City>>(data, HttpStatus.OK);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<List<City>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@GetMapping("/{id}")
    public ResponseEntity<Optional<City>> getByIdCity(@PathVariable Integer id) {
		Optional<City> data = null;
    	try {
    		data = service.getByid(id);
    		if(data.isPresent()) {
        		return new ResponseEntity<Optional<City>>(data, HttpStatus.OK);
    		}else { 
        		return new ResponseEntity<Optional<City>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Optional<City>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> deleteByIdCity(@PathVariable Integer id) {
		Map<String, Object> data =  new HashMap();
		Boolean hasil = false;
    	try {
    		hasil = service.delete(id);
    		if(hasil) {
    			data.put("message", "Deleted Successfully");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.OK);
    		}else { 
    			data.put("message", "Data Not Found");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
			data.put("message", e.getMessage());
    		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/update/{id}")
    public ResponseEntity<City> updateCity(@PathVariable Integer id, @RequestBody City city) {
		City hasil = new City();
    	try {
    		hasil = service.update(id,city);
    		if(hasil != null) { 
        		return new ResponseEntity<City>(hasil, HttpStatus.OK);
    		}else {  
        		return new ResponseEntity<City>(hasil, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<City>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
}
