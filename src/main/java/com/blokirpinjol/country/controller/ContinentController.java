package com.blokirpinjol.country.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blokirpinjol.country.model.Continent;
import com.blokirpinjol.country.service.ContinentService;

@RestController
@RequestMapping("/api/continent")
public class ContinentController {
	Logger logger = LoggerFactory.getLogger(ContinentController.class);
    
    @Autowired
    private ContinentService service;
    
	@PostMapping
    public ResponseEntity<Continent> saveCity(@RequestBody Continent continent) {
		Continent hasil = new Continent();
    	try {
    		hasil = service.save(continent);
    		return new ResponseEntity<Continent>(hasil, HttpStatus.CREATED);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Continent>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    }  
	
	@GetMapping
    public ResponseEntity<List<Continent>> getAllCity() {
		List<Continent> data = new ArrayList<Continent>();
    	try {
    		data = service.getAll();
    		return new ResponseEntity<List<Continent>>(data, HttpStatus.OK);	
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<List<Continent>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@GetMapping("/{id}")
    public ResponseEntity<Optional<Continent>> getByIdCity(@PathVariable Integer id) {
		Optional<Continent> data = null;
    	try {
    		data = service.getByid(id);
    		if(data.isPresent()) {
        		return new ResponseEntity<Optional<Continent>>(data, HttpStatus.OK);
    		}else { 
        		return new ResponseEntity<Optional<Continent>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Optional<Continent>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> deleteByIdCity(@PathVariable Integer id) {
		Map<String, Object> data =  new HashMap();
		Boolean hasil = false;
    	try {
    		hasil = service.delete(id);
    		if(hasil) {
    			data.put("message", "Deleted Successfully");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.OK);
    		}else { 
    			data.put("message", "Data Not Found");
        		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
			data.put("message", e.getMessage());
    		return new ResponseEntity<Map<String, Object>>(data, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
	
	@PostMapping("/update/{id}")
    public ResponseEntity<Continent> updateCity(@PathVariable Integer id, @RequestBody Continent continent) {
		Continent hasil = new Continent();
    	try {
    		hasil = service.update(id,continent);
    		if(hasil != null) { 
        		return new ResponseEntity<Continent>(hasil, HttpStatus.OK);
    		}else {  
        		return new ResponseEntity<Continent>(hasil, HttpStatus.NOT_FOUND);
    		}
    	}catch (Exception e) { 
			logger.error(e.getMessage());
    		return new ResponseEntity<Continent>(hasil, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
    } 
}
